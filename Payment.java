package com.bta;

public interface Payment {
    
    public String getBankAccount();

    public String getAccountOwner();

    public void sendMoney(int amount);
}

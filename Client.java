package com.bta;

public class Client implements Payment {

    private String name;
    private String bankAccount;
    private int accountBalance = 100;

    @Override
    public String getBankAccount() {
        return bankAccount;
    }

    @Override
    public String getAccountOwner() {
        return name;
    }

    @Override
    public void sendMoney(int amount) {
        if(this.accountBalance - amount > 0) {
            this.accountBalance -= amount;
        } else {
            System.out.println("Nepakankamas balansas");
        }
    }



    Client(String name, String accNumber) {
        this.name = name;
        this.bankAccount = accNumber;
    }






}

package com.bta;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Client[] klientai = new Client[3];

        klientai[0] = new Client("Klientas 1", "LT1344");
        klientai[1] = new Client("Klientas 2", "LT3333");
        klientai[2] = new Client("Klientas 3", "LT1111");

        for(int i = 0; i < klientai.length; i++) {
            System.out.println(klientai[i].getAccountOwner() + " " + klientai[i].getBankAccount());
        }
    }
}
